const AUTH_PROVIDER_URL = "http://localhost:1337";
const ACCESS_TOKEN_KEY = "access-token";
const CODE_VERIFIER_KEY = "code-verifier";
const CLIENT_ID = "test-client-id";
const REDIRECT_URI = window.location.origin + '/';

addEventListener('load', async () => {
    const urlParams = new URLSearchParams(window.location.search);
    const code = urlParams.get('code');
    if (code) {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        const raw = JSON.stringify({
            grant_type: "authorization_code",
            code: code,
            redirect_uri: REDIRECT_URI,
            client_id: CLIENT_ID,
            code_verifier: getCodeVerifier()
        });
        const requestOptions: RequestInit = {
            method: 'POST',
            headers: headers,
            body: raw,
        }
        const response = await fetch(`${AUTH_PROVIDER_URL}/token`, requestOptions);
        const text = await response.text();
        const obj: { access_token: string } = JSON.parse(text);
        setAccessToken(obj.access_token);
        opener.postMessage('auth-done');
        close();
    }
});

async function loadProjectList() {
    if (!getAccessToken()) {
        if (confirm('You need to grant authorization')) {
            window.open(createAuthorizationUrl());
            window.addEventListener('message', (evt) => {
                if (evt.data === "auth-done") {
                    getProjects();
                }
            })
        }
    } else {
        getProjects();
    }
}

async function getProjects() {
    const response = await fetch(`${AUTH_PROVIDER_URL}/api/projects`, {
        method: 'GET',
        headers: new Headers({'Authorization': `Bearer ${getAccessToken()}`})
    });
    document.getElementById('projects-container').innerText = await response.text();
}

function getAccessToken() {
    return localStorage.getItem(ACCESS_TOKEN_KEY);
}

function setAccessToken(token: string) {
    localStorage.setItem(ACCESS_TOKEN_KEY, token);
}


function createAuthorizationUrl() {
    const url = new URL('http://localhost:1337/authorize');
    const codeChallenge = createCodeChallenge(getCodeVerifier());
    const {searchParams} = url;
    searchParams.append('response_type', 'code');
    searchParams.append('client_id', CLIENT_ID);
    searchParams.append('redirect_uri', REDIRECT_URI);
    searchParams.append('code_challenge', codeChallenge);
    searchParams.append('code_challenge_method', 'S256');
    return url;
}

function getCodeVerifier(): string {
    let codeVerifier = localStorage.getItem(CODE_VERIFIER_KEY);
    if (!codeVerifier) {
        codeVerifier = createCodeVerifier(128);
        localStorage.setItem(CODE_VERIFIER_KEY, codeVerifier);
    }
    return codeVerifier;
}


function createCodeVerifier(length: number) {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-._~';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

function createCodeChallenge(codeVerifier: string) {
    // @ts-ignore
    return btoa(sha256(codeVerifier));
}
